let totalSoal = 0;
let jawabanBenar = 0;
let jawabanSalah = 0;
let play = true;
let finishSound = new Audio('sound/finish.flac');
let correctSound = new Audio('sound/correct.wav');
let wrongSound = new Audio('sound/wrong.wav');

// finish animation
var mp = 150;
var particleColors = {
    colorOptions: ["DodgerBlue", "OliveDrab", "Gold", "pink", "SlateBlue", "lightblue", "Violet", "PaleGreen", "SteelBlue", "SandyBrown", "Chocolate", "Crimson"],
    colorIndex: 0,
    colorIncrementer: 0,
    colorThreshold: 10
}

$('#paragraf span').on('click', function(){
	let spanval = $(this).data('value');
	if($(this).hasClass('badge')) return;
	if(spanval == 'kerja-mental'){
		$(this).addClass('badge badge-success');
		jawabanBenar++;
		$('.jawaban-benar').html(jawabanBenar);
		animateCSS('.rekap', 'bounceIn');
		if(jawabanBenar == totalSoal){
			$('#successModal').modal('show');
			finishSound.play();
			explode(150,50);
		} else {
    		correctSound.pause();
            correctSound.currentTime = 0;
            correctSound.play();
        }
	} else {
		wrongSound.pause();
        wrongSound.currentTime=0;
        wrongSound.play();
		$(this).addClass('badge badge-warning');
		jawabanSalah++;
		$('#star'+jawabanSalah).remove();
		if(jawabanSalah == 5){
			$('#gameoverModal').modal('show');
		}
	}
})

$('.cobalagi').on('click', function(){
	play = true;
	jawabanSalah = 0;
	jawabanBenar = 0;
	$('.jawaban-benar').html(jawabanBenar);
	$('#paragraf span')
		.removeClass('badge')
		.removeClass('badge-success')
		.removeClass('badge-warning');
	$('.chance').html(`<i id="star1" class="zmdi zmdi-star zmdi-hc-lg mr-1 text-warning"></i>
            <i id="star2" class="zmdi zmdi-star zmdi-hc-lg mr-1 text-warning"></i>
            <i id="star3" class="zmdi zmdi-star zmdi-hc-lg mr-1 text-warning"></i>
            <i id="star4" class="zmdi zmdi-star zmdi-hc-lg mr-1 text-warning"></i>
            <i id="star5" class="zmdi zmdi-star zmdi-hc-lg mr-1 text-warning"></i>`);
	$('#gameoverModal').modal('hide');
	$('#successModal').modal('hide');
	animateCSS('.rekap', 'slideInDown');
	animateCSS('.chance', 'slideInDown');
})

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

$(function(){
	totalSoal = $('span[data-value="kerja-mental"]').length;
	$('.total').html(totalSoal);
})


 //Setup Confetti animation
const colors = [ '#D8589F', '#EE4523', '#FBE75D', '#4FC5DF'];
const bubbles = 80;

const explode = (x, y) => {
	
    let particles = [];
    let ratio = window.devicePixelRatio;
    let c = document.createElement('canvas');
    let ctx = c.getContext('2d');

    c.style.position = 'absolute';
    c.style.left = (x - 100) + 'px';
    c.style.top = (y - 100) + 'px';
    c.style.pointerEvents = 'none';
    c.style.width = 500 + 'px';
    c.style.height = 500 + 'px';
    c.style.zIndex = 9999;
    c.width = 500 * ratio;
    c.height = 500 * ratio;
    document.body.appendChild(c);

    for(var i = 0; i < bubbles; i++) {
        particles.push({
            x: c.width / 2,
            y: c.height / r(2, 4),
            radius: r(3, 8),
            color: colors[Math.floor(Math.random() * colors.length)],
            rotation: r(230, 310, true),
            speed: r(3, 7),
            friction: .99,
            fade: .03,
            opacity: r(100, 100, true),
            yVel: 0,
            gravity: 0.04
        });
    }

    render(particles, ctx, c.width, c.height);
    setTimeout(() => document.body.removeChild(c), 5000);
}

const render = (particles, ctx, width, height) => {
    requestAnimationFrame(() => render(particles, ctx, width, height));
    ctx.clearRect(0, 0, width, height);

    particles.forEach((p, i) => {
        p.x += p.speed * Math.cos(p.rotation * Math.PI / 180);
        p.y += p.speed * Math.sin(p.rotation * Math.PI / 180);

        p.opacity -= 0.005;
        p.speed *= p.friction;
        p.radius -= p.fade;
        p.yVel += p.gravity;
        p.y += p.yVel;

        if(p.opacity < 0 || p.radius < 0) return;

        ctx.beginPath();
        ctx.globalAlpha = p.opacity;
        ctx.fillStyle = p.color;
        ctx.arc(p.x, p.y, p.radius, 0, 2 * Math.PI, false);
        ctx.fill();
    });

    return ctx;
}
const r = (a, b, c) => parseFloat((Math.random() * ((a ? a : 1) - (b ? b : 0)) + (b ? b : 0)).toFixed(c ? c : 0));