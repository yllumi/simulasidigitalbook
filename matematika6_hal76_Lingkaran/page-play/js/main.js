$(function(){
    $('li').on('mouseover', function(){
        let target = $(this).data('target');
        console.log(target);
        if(target == 'luas'){
            $('#luas').css('transform','translate(-20px,-20px)').css('z-index','100').addClass('angkat');
        }
        if(target == 'keliling'){
            $('#keliling').css('transform','translate(-20px,-20px)').css('z-index','100');
        }
        if(target == 'radius'){
            $('#radius').css('top','100px').css('opacity','1');
        }
        if(target == 'diameter'){
            $('#diameter').css('top','100px').css('opacity','1');
        }
    })

    $('li').on('mouseout', function(){
        let target = $(this).data('target');
        console.log(target);
        if(target == 'luas'){
            $('#luas').css('transform','translate(0,0)').css('z-index','1').removeClass('angkat');
        }
        if(target == 'keliling'){
            $('#keliling').css('transform','translate(0,0)').css('z-index','1');
        }
        if(target == 'radius'){
            $('#radius').css('top','110px').css('opacity','0');
        }
        if(target == 'diameter'){
            $('#diameter').css('top','110px').css('opacity','0');
        }
    })
})