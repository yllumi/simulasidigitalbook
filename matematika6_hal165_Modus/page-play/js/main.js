let currentSoal = 0;
let jawabanBenar = 1;
let jawaban;
let play = true;
let correctSound = new Audio('sound/correct.wav');
let wrongSound = new Audio('sound/wrong.wav');
let chance = 0;

function cekJawaban(pilihan)
{
	$('.jawaban').text(pilihan);

	if(jawabanBenar == parseInt(pilihan)){
		$('#salah').addClass('sr-only');
		$('#benar').removeClass('sr-only');
		animateCSS('#benar', 'bounceIn');
		correctSound.play();
		$('#garis-bilangan button').prop('disabled',true);
		$('.btn-options').removeClass('sr-only');
		play = false;
	} else {
		wrongSound.pause();
		wrongSound.currentTime=0;
		$('#benar').addClass('sr-only');
		$('#salah').removeClass('sr-only');
		animateCSS('#salah', 'swing');
		wrongSound.play();
		chance++;
		if(chance >= 3){
			$('.btn-options').removeClass('sr-only');
		}
	}
}

function nextSoal()
{
	currentSoal++;
	if(soal.length <= currentSoal) currentSoal = 0;
	showSoal();
}

function showSoal()
{
	play = true;
	$('#table').html(soal[currentSoal].table);
	let pilihan = soal[currentSoal].pilihan;
	let jawaban = soal[currentSoal].jawaban;
	$('#pilihan').empty();
	pilihan.forEach(function(item, index){
		$('#pilihan').append(`<button class="btn btn-success btn-pilihan mb-2" data-answer="${item==jawaban?1:0}" style="width:75px">${item}</button><br>`)
	});
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

$('#pilihan').on('click', 'button', function(){
	if(play){
		pilihan = $(this).data('answer');
		if(pilihan == 1){
			correctSound.pause();
			correctSound.currentTime = 0;
			$('#salah').addClass('sr-only');
			$('#benar').removeClass('sr-only');
			correctSound.play();
			animateCSS('#benar', 'bounceIn');
			$('#nextSoal').removeClass('sr-only');
			$(this).removeClass('btn-success').addClass('btn-primary');
			$('.btn-pilihan').addClass('disabled').prop('disabled',true);
			play = false;
		} else {
			$(this).removeClass('btn-success').addClass('btn-warning').prop('disabled',true);
			wrongSound.pause();
			wrongSound.currentTime = 0;
			$('#benar').addClass('sr-only');
			$('#salah').removeClass('sr-only');
			wrongSound.play();
			animateCSS('#salah', 'swing');
		}
	} 
})
$('#nextSoal').on('click', function(){
	$(this).addClass('sr-only');
	$('#benar').addClass('sr-only');
	$('#salah').addClass('sr-only');
	nextSoal();
})

$(() => showSoal())