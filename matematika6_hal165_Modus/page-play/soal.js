var soal = [
	{
		"table": `<table class="table table-bordered border-0 table-sm mx-auto mb-3" style="width:80%">
          <tr>
            <th>Nilai</th>
            <th>Banyak Siswa</th>
          </tr>
          <tr><td>6</td><td>3</td></tr>
          <tr><td>7</td><td>7</td></tr>
          <tr><td>8</td><td>6</td></tr>
          <tr><td>9</td><td>6</td></tr>
          <tr><td>10</td><td>4</td></tr>
        </table>`,
		"pilihan": [6,7,8,9,10],
		"jawaban": 7		
	},
	{
		"table": `<table class="table table-bordered border-0 table-sm mx-auto mb-3" style="width:80%">
          <tr>
            <th>Nilai</th>
            <th>Banyak Siswa</th>
          </tr>
          <tr><td>7</td><td>8</td></tr>
          <tr><td>8</td><td>9</td></tr>
          <tr><td>9</td><td>7</td></tr>
          <tr><td>10</td><td>3</td></tr>
        </table>`,
		"pilihan": [7,8,9,10],
		"jawaban": 8
	},
	{
		"table": `<table class="table table-bordered border-0 table-sm mx-auto mb-3" style="width:80%">
          <tr>
            <th>Nilai</th>
            <th>Banyak Siswa</th>
          </tr>
          <tr><td>6</td><td>6</td></tr>
          <tr><td>7</td><td>9</td></tr>
          <tr><td>8</td><td>12</td></tr>
          <tr><td>9</td><td>6</td></tr>
        </table>`,
		"pilihan": [6,7,8,9],
		"jawaban": 8
	},
	{
		"table": `<table class="table table-bordered border-0 table-sm mx-auto mb-3" style="width:80%">
          <tr>
            <th>Nilai</th>
            <th>Banyak Siswa</th>
          </tr>
          <tr><td>75</td><td>10</td></tr>
          <tr><td>80</td><td>15</td></tr>
          <tr><td>85</td><td>18</td></tr>
          <tr><td>90</td><td>24</td></tr>
          <tr><td>95</td><td>12</td></tr>
        </table>`,
		"pilihan": [75,80,85,90,95],
		"jawaban": 90
	}
];