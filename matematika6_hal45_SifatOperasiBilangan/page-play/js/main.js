let correctSound = new Audio('sound/correct.wav');
let wrongSound = new Audio('sound/wrong.wav');
let complete = 3;
let totalSoal = soal.length;
let currentSoal;
let indexSoalShuffle;
var indexSoalMuncul;

$(function(){
    currentSoal = getCurrentSoal();

    prepareSoal(currentSoal);

    $('.wadah').droppable({
      drop: function( event, ui ) {
        $(this).attr('data-answer', ui.draggable.data('sifat'));

        var drop_p = $(this).offset();
        var drag_p = ui.draggable.offset();
        var left_end = drop_p.left - drag_p.left + 5;
        var top_end = drop_p.top - drag_p.top + 13;
        ui.draggable.animate({
            top: '+=' + top_end,
            left: '+=' + left_end
        });
      },
    });

	$('.cobalagi').on('click', function(){
        location.reload();
	})

    $('#btnCek').on('click', function(){
        let jawabanBetul = 0;
        console.log(jawabanBetul);
        $(document).find('.wadah').each(function(){
            if($(this).data('answer') == $(this).data('alamat')){
                $(this).removeClass('bg-light bg-success bg-warning').addClass('bg-success');
                jawabanBetul++;
            }
            else
                $(this).removeClass('bg-light bg-success bg-warning').addClass('bg-warning');
        })
        if(jawabanBetul == 3)
            $('#successModal').modal('show');
        else
            $('#gameoverModal').modal('show');
    })
})

function prepareSoal(nomorSoal)
{
    $('#asal').append(`<div class="badge shadowed badge-secondary" data-sifat="${soal[nomorSoal][0][0]}">${soal[nomorSoal][0][1]}</div>`);
    $('#asal').append(`<div class="badge shadowed badge-secondary" data-sifat="${soal[nomorSoal][1][0]}">${soal[nomorSoal][1][1]}</div>`);
    $('#asal').append(`<div class="badge shadowed badge-secondary" data-sifat="${soal[nomorSoal][2][0]}">${soal[nomorSoal][2][1]}</div>`);

    $(document).find( ".badge" ).draggable(
        {cursor: "move"}
    );
}

function getCurrentSoal()
{
    let currentSoal = localStorage.getItem('currentSoal-mat2') || 0;
    currentSoal++;
    if(currentSoal >= totalSoal)
        currentSoal = 0;
    localStorage.setItem('currentSoal-mat2', currentSoal);
    return currentSoal;
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

 //Setup Confetti animation
const colors = [ '#D8589F', '#EE4523', '#FBE75D', '#4FC5DF'];
const bubbles = 80;

const explode = (x, y) => {
	
    let particles = [];
    let ratio = window.devicePixelRatio;
    let c = document.createElement('canvas');
    let ctx = c.getContext('2d');

    c.style.position = 'absolute';
    c.style.left = (x - 100) + 'px';
    c.style.top = (y - 100) + 'px';
    c.style.pointerEvents = 'none';
    c.style.width = 500 + 'px';
    c.style.height = 500 + 'px';
    c.style.zIndex = 9999;
    c.width = 500 * ratio;
    c.height = 500 * ratio;
    document.body.appendChild(c);

    for(var i = 0; i < bubbles; i++) {
        particles.push({
            x: c.width / 2,
            y: c.height / r(2, 4),
            radius: r(3, 8),
            color: colors[Math.floor(Math.random() * colors.length)],
            rotation: r(230, 310, true),
            speed: r(3, 7),
            friction: .99,
            fade: .03,
            opacity: r(100, 100, true),
            yVel: 0,
            gravity: 0.04
        });
    }

    render(particles, ctx, c.width, c.height);
    setTimeout(() => document.body.removeChild(c), 5000);
}

const render = (particles, ctx, width, height) => {
    requestAnimationFrame(() => render(particles, ctx, width, height));
    ctx.clearRect(0, 0, width, height);

    particles.forEach((p, i) => {
        p.x += p.speed * Math.cos(p.rotation * Math.PI / 180);
        p.y += p.speed * Math.sin(p.rotation * Math.PI / 180);

        p.opacity -= 0.005;
        p.speed *= p.friction;
        p.radius -= p.fade;
        p.yVel += p.gravity;
        p.y += p.yVel;

        if(p.opacity < 0 || p.radius < 0) return;

        ctx.beginPath();
        ctx.globalAlpha = p.opacity;
        ctx.fillStyle = p.color;
        ctx.arc(p.x, p.y, p.radius, 0, 2 * Math.PI, false);
        ctx.fill();
    });

    return ctx;
}
const r = (a, b, c) => parseFloat((Math.random() * ((a ? a : 1) - (b ? b : 0)) + (b ? b : 0)).toFixed(c ? c : 0));