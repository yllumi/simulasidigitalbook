let soal = [
	[
		["asosiatif","(5 + 2) + 4 = 4 + (2 + 5)"],
		["komutatif","3 + 4 = 4 + 3"],
		["distributif","7 x (5 + 6) = (7 x 5) + (7 x 6)"],
	],
	[
		["distributif","(7 + 5) x 6 = (7 x 6) + (5 x 6)"],
		["asosiatif","(5 + 7) + 3 = 3 + (7 + 5)"],
		["komutatif","2 + 9 = 9 + 2"],
	],
	[
		["asosiatif","(5 + 2) + 4 = 4 + (2 + 5)"],
		["distributif","8 x (5 + 6) = (8 x 5) + (8 x 6)"],
		["komutatif","5 + 4 = 4 + 5"],
	],

];