let soal=[];
let currentSoal = 0;
let jawabanBenar = 1;
let jawaban;
let play = true;
let correctSound = new Audio('sound/correct.wav');
let wrongSound = new Audio('sound/wrong.wav');
let chance = 0;

function cekJawaban(pilihan)
{
	$('.jawaban').text(pilihan);

	if(jawabanBenar == parseInt(pilihan)){
		$('#salah').addClass('sr-only');
		$('#benar').removeClass('sr-only');
		animateCSS('#benar', 'bounceIn');
		correctSound.play();
		$('#garis-bilangan button').prop('disabled',true);
		$('.btn-options').removeClass('sr-only');
		play = false;
	} else {
		wrongSound.pause();
		wrongSound.currentTime=0;
		$('#benar').addClass('sr-only');
		$('#salah').removeClass('sr-only');
		animateCSS('#salah', 'swing');
		wrongSound.play();
		chance++;
		if(chance >= 3){
			$('.btn-options').removeClass('sr-only');
		}
	}
}

function nextSoal()
{
	currentSoal++;
	showSoal();
}

function showSoal()
{
	jawabanBenar = soal[currentSoal].answer;
	$('#operand1').text(soal[currentSoal].operand1);
	$('#operand2').text(soal[currentSoal].operand2);
	$('#operator').text(soal[currentSoal].operator);
	$('.jawaban').text('?');
	$('#benar').addClass('sr-only');
	$('#salah').addClass('sr-only');
	$('#garis-bilangan button').prop('disabled',false);
	$('.btn-options').addClass('sr-only');
	$('#videoSoal').find('source').attr('src', 'animation/' + soal[currentSoal].animation);
	document.getElementById('videoSoal').load();
	chance = 0;
	play = true;
}

function loadJSONSoal()
{
	$.getJSON('soal.json', function(data){
		soal = data;
		currentSoal = 0;
		showSoal();
	})
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

$('#garis-bilangan').find('button').on('click', function(){
	if(play){
		pilihan = $(this).data('value');
		cekJawaban(pilihan);
	} 
})
$('#nextSoal').on('click', function(){
	nextSoal();
})
$('#videoModal').on('shown.bs.modal', function (e) {
  document.getElementById('videoSoal').play();
})
$('#videoModal').on('hide.bs.modal', function (e) {
  document.getElementById('videoSoal').pause();
})

$(() => loadJSONSoal())