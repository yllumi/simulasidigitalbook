let correctSound = new Audio('sound/correct.wav');
let wrongSound = new Audio('sound/wrong.wav');
let finishSound = new Audio('sound/finish.flac');

let totalKata = $('.options').length;

$(function(){
    
})

$('.options button').on('click', function(){
    $(this).siblings().removeClass('btn-primary').addClass('btn-secondary');
    $(this).addClass('btn-primary').removeClass('btn-secondary');
    $(this).parents('.options').find('.option-caption').html($(this).data('caption'));
    $(this).parents('.options').data('value', $(this).data('value'));
})

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

$('.btnReset').on('click', function(){
    location.reload();
});

$('.btnCheck').on('click', function(){
    let totalBenar = 0;
    console.log('Total kata: ' + totalKata);

    $('.options').each(function(i,e){
        let target = $(this).data('target');
        let value = $(this).data('value');
        console.log(target + " " + value);
    
        if(target == value)
            totalBenar++;
    });

    console.log('Total benar: ' + totalBenar);


    if(totalBenar == totalKata){
        $('#salah').addClass('sr-only');
        $('#benar').removeClass('sr-only');
        animateCSS('#benar', 'bounceIn');
        finishSound.play();
        explode(0, 100)
    } else {
        $('#salah').removeClass('sr-only');
        animateCSS('#salah', 'jello');
        wrongSound.play();
    }
})

 //Setup Confetti animation
const colors = [ '#D8589F', '#EE4523', '#FBE75D', '#4FC5DF'];
const bubbles = 80;

const explode = (x, y) => {
	
    let particles = [];
    let ratio = window.devicePixelRatio;
    let c = document.createElement('canvas');
    let ctx = c.getContext('2d');

    c.style.position = 'absolute';
    c.style.left = (x - 100) + 'px';
    c.style.top = (y - 100) + 'px';
    c.style.pointerEvents = 'none';
    c.style.width = 500 + 'px';
    c.style.height = 500 + 'px';
    c.style.zIndex = 9999;
    c.width = 500 * ratio;
    c.height = 500 * ratio;
    document.body.appendChild(c);

    for(var i = 0; i < bubbles; i++) {
        particles.push({
            x: c.width / 2,
            y: c.height / r(2, 4),
            radius: r(3, 8),
            color: colors[Math.floor(Math.random() * colors.length)],
            rotation: r(230, 310, true),
            speed: r(3, 7),
            friction: .99,
            fade: .03,
            opacity: r(100, 100, true),
            yVel: 0,
            gravity: 0.04
        });
    }

    render(particles, ctx, c.width, c.height);
    setTimeout(() => document.body.removeChild(c), 5000);
}

const render = (particles, ctx, width, height) => {
    requestAnimationFrame(() => render(particles, ctx, width, height));
    ctx.clearRect(0, 0, width, height);

    particles.forEach((p, i) => {
        p.x += p.speed * Math.cos(p.rotation * Math.PI / 180);
        p.y += p.speed * Math.sin(p.rotation * Math.PI / 180);

        p.opacity -= 0.005;
        p.speed *= p.friction;
        p.radius -= p.fade;
        p.yVel += p.gravity;
        p.y += p.yVel;

        if(p.opacity < 0 || p.radius < 0) return;

        ctx.beginPath();
        ctx.globalAlpha = p.opacity;
        ctx.fillStyle = p.color;
        ctx.arc(p.x, p.y, p.radius, 0, 2 * Math.PI, false);
        ctx.fill();
    });

    return ctx;
}
const r = (a, b, c) => parseFloat((Math.random() * ((a ? a : 1) - (b ? b : 0)) + (b ? b : 0)).toFixed(c ? c : 0));