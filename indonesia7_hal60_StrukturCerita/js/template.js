var el;
var elWidth;
var elHeight;
var bodyWidth = $(window).width();
var bodyHeight = $(window).height();

$(window).load(function() {
  $(".se-pre-con").fadeOut("slow");
  doResize();
});

$(window).resize(function() {
  doResize();
});

$(document).ready(function(){
  doResize();
})

function doResize() {
  elWidth = $(".scaled-container").outerWidth() + 50;
  elHeight = $(".scaled-container").outerHeight() + 50;

  var scale;
  bodyWidth = $(window).width();
  bodyHeight = $(window).height();
    
  scale = Math.min(
    bodyWidth / elWidth,    
    bodyHeight / elHeight
  );

  $(".scaled-container").css("transform", "translate(-50%, -50%) scale(" + scale + ")");
}